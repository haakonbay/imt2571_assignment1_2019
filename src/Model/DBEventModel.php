<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing
  * data in a MySQL database using PDO.
  * @author Rune Hjelsvold
  * @see http://php-html.net/tutorials/model-view-controller-in-php/
  *      The tutorial code used as basis.
  */

require_once("AbstractEventModel.php");
require_once("Event.php");
require_once("dbParam.php");

/** The Model is the class holding data about a archive of events.
  * @todo implement class functionality.
  */
  class DBEventModel extends AbstractEventModel
  {
      protected $db = null;

      /**
        * @param PDO $db PDO object for the database; a new one will be created if
        *                no PDO object is passed
        * @todo Complete the implementation using PDO and a real database.
        * @throws PDOException
        */
      public function __construct($db = null)
      {
          if ($db) {
              $this->db = $db;
          } else {
              // TODO: Create PDO connection
              try {
                  $this->db = new PDO(
                        'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8',
                        DB_USER, DB_PWD,
                        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                              PDO::ATTR_EMULATE_PREPARES   => false)
                    );
              } catch (\PDOException $e) {
                   throw new \PDOException($e->getMessage(), (int)$e->getCode());
              }
          }
      }

      /** Function returning the complete list of events in the archive. Events
        * are returned in order of id.
        * @return Event[] An array of event objects indexed and ordered by id.
        * @todo Complete the implementation using PDO and a real database.
        * @throws PDOException
        */
      public function getEventArchive()
      {
          $eventList = array();

          // TODO: Retrive events from the database and add to the list, one by one
          $stmt = $this->db->query('SELECT * From event');

          while ($row = $stmt->fetch())
          {
              $event = new Event($row[1],$row[2],$row[3],$row[0]);
              array_push($eventList, $event);
          }


          return $eventList;
      }

    /** Function retrieving information about a given event in the archive.
      * @param integer $id the id of the event to be retrieved
      * @return Event|null The event matching the $id exists in the archive;
      *         null otherwise.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      */
    public function getEventById($id)
    {
        $event = null;

        // TODO: Retrive the event from the
        Event::verifyId($id);
        $stmt = $this->db->prepare('SELECT * From event Where id = ?');
        $stmt->execute([$id]);
        $row = $stmt->fetch();
        $event = new Event($row[1],$row[2],$row[3],$row[0]);

        return $event;

    }

    /** Adds a new event to the archive.
      * @param Event $event The event to be added - the id of the event will be set after successful insertion.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
      */
    public function addEvent($event)
    {
        // TODO: Add the event to the database
        $event->verify(true);

        $this->db->exec("INSERT INTO event(title, date, description) VALUES ("$event->title","$event->date","$event->description")");



    }

    /** Modifies data related to a event in the archive.
      * @param Event $event The event data to be kept.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
     */
    public function modifyEvent($event)
    {
        // TODO: Modify the event in the database
    }

    /** Deletes data related to a event from the archive.
      * @param $id integer The id of the event that should be removed from the archive.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
     */
    public function deleteEvent($id)
    {
        // TODO: Delete the event from the database
    }
}
